Tasty Ratios

Bluetooth Enabled Android App

Tasty Ratios is an Android App I developed along with my team (Ctrl-Alt-Beleive) for our COMP 3900 Coumputer Projects Practicum course and Reflex Wireless. Reflex Wireless designed and built NutriCrystal, a digital food scale that can communicate with mobile devices via Bluetooth.

They wanted to develop a demo-able Android mobile application that will enable the user to interact with their digital scale with an Android device.

Proposed Solution

We (Ctrl-Alt-Believe) proposed to develop a demo-able Android mobile application that will give the user the ability to create, share and use recipes (by interacting with the scale).

The user will be able to specify recipe ingredient, measurements, associated steps, and the app will have the ability to communicate with a NutriCrystal scale and show the user live progress of how much of the ingredient being weighed on the scale is over or under as needed.

Project Goals, Tasks & Delivered Features

The end goal of Tasty Ratios is to produce a demo-able android app. The app user will be able to login using either their social media accounts (Facebook, Twitter or
Pinterest). They will be able to specify the quantity of each ingredient in the recipes they create and add a picture to each step of the recipe. Created recipes can then be uploaded to Reflex Wireless’s remote database where other users of the app can download it to their device.

Shared recipes can be followed step by step with steps requiring ingredient measurements showing live progress of that ingredient being weighed on a NutriCrystal scale. The users will also have the ability to share their recipes they followed or created to Facebook, Twitter and Pinterest. Reflex Wireless would like to develop a bare bones demo-able Android mobile application with the following specifications:

    Users will be able to register / login (using Facebook, Twitter or Pinterest)
        The user will be presented with a simple login interface to login to the app with social media accounts
    Users can search for recipes by entering the food title (the app will go parse a website and store them to the remote database)
        The recipe will be searched from a single website
        Parsing will be done on device
        The parsed recipe will be stored onto the device
    Users can create recipes and store them on device or share to social media(Facebook & Twitter)
        Recipes will include ingredient measurements
        Each social media post will include will include a short excerpt about the recipe posted
        Each recipe step will be limited to 140 characters
        Each post can contain a picture
    Users can download shared recipes from Reflex Wireless’s remote database andfollow along in the app
        Each step in the recipe will be shown with a swipe left or right
        When there is an ingredient measurement associated with a step, the user will have the ability to get live feedback from the scale on the amount being measured with a progress bar indicating the ratio
    Users can create custom nutrition information for different ingredients on device and on external database
    Every function in the app will be reachable in 3 steps

