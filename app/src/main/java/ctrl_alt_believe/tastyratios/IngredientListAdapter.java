package ctrl_alt_believe.tastyratios;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by brycen on 14-11-20.
 */
public class IngredientListAdapter extends ArrayAdapter<Ingr_DB_MDL> {

    public LayoutInflater inflater;
    public Activity activity;

    private ArrayList<Ingr_DB_MDL> list;

    public IngredientListAdapter(Context context, int resource, ArrayList<Ingr_DB_MDL> list) {
        super(context, resource, list);
        this.list = list;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        if(convertView == null) {
            inflater = (LayoutInflater) getContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = inflater.inflate(R.layout.ingredient_list_item, null);
        }

        final Ingr_DB_MDL i = list.get(position);

        TextView name = (TextView) convertView.findViewById(R.id.ingredient_name);
        TextView amount = (TextView) convertView.findViewById(R.id.ingredient_amount);
        TextView unit = (TextView) convertView.findViewById(R.id.ingredient_unit);

        name.setText(i.getName());
        amount.setText(i.getAmount());
        unit.setText(i.getUnit());

        return convertView;
    }

}
