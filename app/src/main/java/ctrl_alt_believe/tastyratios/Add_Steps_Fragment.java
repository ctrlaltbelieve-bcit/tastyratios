package ctrl_alt_believe.tastyratios;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import it.gmariotti.cardslib.library.internal.Card;
import it.gmariotti.cardslib.library.internal.CardArrayAdapter;
import it.gmariotti.cardslib.library.internal.CardHeader;
import it.gmariotti.cardslib.library.view.CardListView;

public class Add_Steps_Fragment extends Fragment {

    View rootView;
    CardListView steps_listView;
    ArrayList<Card> cards = new ArrayList<Card>();
    CardArrayAdapter mCardArrayAdapter;
    ArrayList<Steps_DB_MDL> stepArr = new ArrayList<Steps_DB_MDL>();
    TextView actionBar;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        rootView = inflater.inflate(R.layout.fragment_add__steps, container, false);

        steps_listView = (CardListView) rootView.findViewById(R.id.clv_steps_list);

        actionBar = (TextView) rootView.findViewById(R.id.tvAdd_Step_ActionBar);
        return rootView;

    }

    public void createStepCard(String step, Recipe_DB_MDL r ){

        //Create Step DB object and add to array list
        int x = cards.size() + 1;
        Steps_DB_MDL current = new Steps_DB_MDL(r.getRID(), x, step);
        stepArr.add(current);

        Card newCard = new Card(getActivity());
        CardHeader header = new CardHeader(getActivity());

        header.setTitle("Step " + x);
        newCard.addCardHeader(header);

        newCard.setTitle(step);

        newCard.setSwipeable(true);
        cards.add(newCard);

        newCard.setOnSwipeListener(new Card.OnSwipeListener() {
            @Override
            public void onSwipe(Card card) {
                cards.remove(card);
                for ( int i = 0; i < cards.size() ; i++)
                {
                    cards.get(i).setTitle("Step" + (i + 1) );
                }
                mCardArrayAdapter = new CardArrayAdapter(getActivity(), cards);
                mCardArrayAdapter.notifyDataSetChanged();
                steps_listView.setAdapter(mCardArrayAdapter);

            }
        });

        mCardArrayAdapter = new CardArrayAdapter(getActivity(), cards);
        steps_listView.setAdapter(mCardArrayAdapter);

    }

    public void setTitle( String recipeName )
    {
        actionBar.setText(recipeName);
    }

    public ArrayList<Steps_DB_MDL> getStepArr(){
        return stepArr;
    }
}
