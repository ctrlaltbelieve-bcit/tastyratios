package ctrl_alt_believe.tastyratios;

import android.app.Activity;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.Serializable;


public class MeasureIngr extends Activity implements Serializable {


    public static final int MESSAGE_STATE_CHANGE = 1;
    public static final int MESSAGE_READ = 2;
    public static final int MESSAGE_WRITE = 3;
    public static final int MESSAGE_DEVICE_NAME = 4;
    public static final int MESSAGE_TOAST = 5;

    public static final String DEVICE_NAME = "device_name";
    public static final String TOAST = "toast";

    private static final int REQUEST_CONNECT_DEVICE = 2;
    private static final int REQUEST_ENABLE_BT = 3;

   private TextView ingr_name, ingr_amnt;


    private String mConnectedDeviceName = null;

    private StringBuffer mOutStringBuffer;

    private BluetoothAdapter mBluetoothAdapter = null;

    private BluetoothService mService = null;

    private ProgressBar progressBar;

    private int progress;
    private Ingr_DB_MDL ingr;
    private int total;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);



        setContentView(R.layout.activity_measure_ingr);

        mBluetoothAdapter = BluetoothAdapter.getDefaultAdapter();

        if (mBluetoothAdapter == null)
        {
            Toast.makeText(this, "Bluetooth is not available",
                    Toast.LENGTH_LONG).show();
            finish();
            return;
        }

        setTitle("Measuring Ingredient");

        ingr =  getIntent().getParcelableExtra("Ingredient");

        if ( ingr == null)
        {
            Log.e( "INGREDIENT", "INGR IS NULL");
        }

        ingr_name = (TextView) findViewById(R.id.tvMeasure_ingr_name);
        ingr_amnt = (TextView) findViewById(R.id.tvMeasure_ingr_amnt);

        ingr_name.setText( ingr.getName( ) );
        String amount = "                                            0/"
                + ingr.getAmount( ) + " " + ingr.getUnit() ;
        ingr_amnt.setText(amount);

        total = 0;
    }

    @Override
    public void onStart() {
        super.onStart();

        if (!mBluetoothAdapter.isEnabled()) {
            Intent enableIntent = new Intent(
                    BluetoothAdapter.ACTION_REQUEST_ENABLE);
            startActivityForResult(enableIntent, REQUEST_ENABLE_BT);
            // Otherwise, setup the chat session
        } else {
            if (mService == null)
            {
                setupGUI();
            }
        }

    }

    @Override
    public synchronized void onResume() {
        super.onResume();

        if (mService != null) {
            if (mService.getState() == BluetoothService.STATE_NONE) {
                mService.start();
            }
        }
    }

    private void setupGUI() {

        mService = new BluetoothService(this, mHandler);
        mOutStringBuffer = new StringBuffer("");
        progressBar = (ProgressBar) findViewById(R.id.pgWeight);
        progress=0;
        Double tmp = Double.parseDouble( ingr.getAmount( ) );
        progressBar.setMax( tmp.intValue( ) );
    }

    public void onDestroy() {
        super.onDestroy();
        if (mService != null)
            mService.stop();
    }

    private void ensureDiscoverable() {
        if (mBluetoothAdapter.getScanMode() != BluetoothAdapter.SCAN_MODE_CONNECTABLE_DISCOVERABLE) {
            Intent discoverableIntent = new Intent(
                    BluetoothAdapter.ACTION_REQUEST_DISCOVERABLE);
            discoverableIntent.putExtra(
                    BluetoothAdapter.EXTRA_DISCOVERABLE_DURATION, 300);
            startActivity(discoverableIntent);
        }
    }

    // The Handler that gets information back from the MeasureIngrService
    private final Handler mHandler = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
                case MESSAGE_STATE_CHANGE:
                    switch (msg.arg1) {
                        case BluetoothService.STATE_CONNECTED:
                            break;
                        case BluetoothService.STATE_CONNECTING:
                            break;
                        case BluetoothService.STATE_LISTEN:
                        case BluetoothService.STATE_NONE:
                            break;
                    }
                    break;
                case MESSAGE_WRITE:
                    byte[] writeBuf = (byte[]) msg.obj;
                    // construct a string from the buffer
                    String writeMessage = new String(writeBuf);
                    break;
                case MESSAGE_READ:
                    byte[] readBuf = (byte[]) msg.obj;
                    String readMessage = new String(readBuf, 0, msg.arg1);
                    progress = Integer.parseInt(readMessage);
                    progressBar.setProgress(progress);
                    Double tmp = Double.parseDouble( ingr.getAmount( ) );
                    if ( progress > tmp.intValue ( ) )
                    {
                        Drawable drawable = progressBar.getProgressDrawable();
                        //drawable.setColorFilter(new LightingColorFilter(0xFF000000, (400) ) );
                        progressBar.getProgressDrawable().setColorFilter(Color.RED, PorterDuff.Mode.MULTIPLY );
                    }
                    else
                    {
                        Drawable drawable = progressBar.getProgressDrawable();
                        //drawable.setColorFilter(new LightingColorFilter(0xFF000000, (2000) ) );
                        progressBar.getProgressDrawable().setColorFilter(Color.GREEN, PorterDuff.Mode.MULTIPLY );
                    }

                    String amount = "                                            " + progress
                            + "/" + tmp.intValue( ) + " " + ingr.getUnit() ;
                    ingr_amnt.setText( amount );
                    break;
                case MESSAGE_DEVICE_NAME:
                    // save the connected device's name
                    mConnectedDeviceName = msg.getData().getString(DEVICE_NAME);
                    Toast.makeText(getApplicationContext(),
                            "Connected to " + mConnectedDeviceName,
                            Toast.LENGTH_SHORT).show();
                    break;
                case MESSAGE_TOAST:
                    Toast.makeText(getApplicationContext(),
                            msg.getData().getString(TOAST), Toast.LENGTH_SHORT)
                            .show();
                    break;
            }
        }
    };

    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {

        switch (requestCode)
        {
            case REQUEST_CONNECT_DEVICE:

                if (resultCode == Activity.RESULT_OK)
                {
                    connectDevice(data);
                }
                break;
            case REQUEST_ENABLE_BT:

                if (resultCode == Activity.RESULT_OK) {
                    setupGUI();
                } else {

                    Toast.makeText(this, "BT not enabled",
                            Toast.LENGTH_SHORT).show();
                    finish();
                }
        }
    }

    private void connectDevice(Intent data) {
        String address = data.getExtras().getString(
                DeviceListActivity.EXTRA_DEVICE_ADDRESS);
        BluetoothDevice device = mBluetoothAdapter.getRemoteDevice(address);
        mService.connect(device);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.measure, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        Intent serverIntent = null;
        switch (item.getItemId()) {
            case R.id.connect_scan:
                serverIntent = new Intent(this, DeviceListActivity.class);
                startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE);
                return true;
            case R.id.discoverable:
                ensureDiscoverable();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public void scanBT( View v)
    {
        Intent serverIntent = new Intent(this, DeviceListActivity.class);
        startActivityForResult(serverIntent, REQUEST_CONNECT_DEVICE);
    }

    public void discoverable( View v)
    {
        ensureDiscoverable();
    }
}
