package ctrl_alt_believe.tastyratios;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by sparqy on 07/10/14.
 */
public class Ingr_DB_MDL implements Parcelable {

    int IID;
    int RID;
    String name, unit, amount;

    public Ingr_DB_MDL(){}

    public Ingr_DB_MDL(Parcel in){
        IID = in.readInt();
        RID = in.readInt();
        name = in.readString();
        unit = in.readString();
        amount = in.readString();
    }

    public Ingr_DB_MDL(String name){
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public int getIID() {
        return IID;
    }

    public void setIID(int IID) {
        this.IID = IID;

    }
    public int getRID() {
        return RID;
    }

    public void setRID(int RID) {
        this.RID = RID;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getUnit() {return unit; }

    public void setUnit(String unit) {this.unit = unit;}

    public String getAmount() {return amount;}

    public void setAmount(String amount) {
        double amt = 0;
        try {
            amt = Double.parseDouble(amount);
            this.amount = Double.toString(amt);
        } catch (NumberFormatException e) {
            String[] frac = amount.split("(\\s|\\/)");
            if (frac.length == 2) {
                amt = Double.parseDouble(frac[0]) / Double.parseDouble(frac[1]);
            }
            else if (frac.length == 3) {
                amt = Double.parseDouble(frac[0]) + (Double.parseDouble(frac[1]) / Double.parseDouble(frac[2]));
            }
        }
        this.amount = Double.toString(amt);
    }

    @Override
    public int describeContents() {
        return 0;
    }

    public String toString() {
        return name;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(IID);
        dest.writeInt(RID);
        dest.writeString(name);
        dest.writeString(unit);
        dest.writeString(amount);
    }

    public static final Parcelable.Creator<Ingr_DB_MDL> CREATOR = new Parcelable.Creator<Ingr_DB_MDL>() {
        public Ingr_DB_MDL createFromParcel(Parcel in) {
            return new Ingr_DB_MDL(in);
        }

        public Ingr_DB_MDL[] newArray(int size) {
            return new Ingr_DB_MDL[size];
        }
    };
}
