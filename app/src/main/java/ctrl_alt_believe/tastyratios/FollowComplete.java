package ctrl_alt_believe.tastyratios;

import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Toast;
import com.facebook.UiLifecycleHelper;

import com.facebook.model.GraphObject;
import com.facebook.model.OpenGraphAction;
import com.facebook.model.OpenGraphObject;
import com.facebook.widget.FacebookDialog;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import twitter4j.StatusUpdate;
import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.auth.AccessToken;
import twitter4j.conf.ConfigurationBuilder;


public class FollowComplete extends Fragment {

    private OnFragmentInteractionListener mListener;
    private String recipeName;
    private ArrayList<Ingr_DB_MDL> ingr_list;
    private ArrayList<Steps_DB_MDL> step_list;
    private boolean isLocalRecipe;
    private Dialog tDialog;
    private ProgressDialog progress;
    private String tweetText;
    private EditText tweet_text;
    private UiLifecycleHelper uiHelper;
    private SharedPreferences pref;

    public FollowComplete() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        uiHelper = new UiLifecycleHelper(getActivity(), null);
        uiHelper.onCreate(savedInstanceState);

    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        uiHelper.onActivityResult(requestCode, resultCode, data, new FacebookDialog.Callback() {
            @Override
            public void onError(FacebookDialog.PendingCall pendingCall, Exception error, Bundle data) {
                Log.e("Activity", String.format("Error: %s", error.toString()));
            }

            @Override
            public void onComplete(FacebookDialog.PendingCall pendingCall, Bundle data) {
                Log.i("Activity", "Success!");
            }
        });
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        if (getArguments() != null) {
            final Bundle args = getArguments();
            recipeName = args.getString("Title");
            ingr_list = args.getParcelableArrayList("Ingredients");
            step_list = args.getParcelableArrayList("Steps");
            isLocalRecipe = args.getBoolean("Local");
        }

        FrameLayout l = (FrameLayout) inflater.inflate(R.layout.fragment_follow_complete, container, false);
        pref = PreferenceManager.getDefaultSharedPreferences(getActivity());
        ImageView tweet = (ImageView) l.findViewById(R.id.tweet);
        tweet.setOnClickListener(new Tweet());
        final Button back = (Button) l.findViewById(R.id.btn_complete_back);
        final Button save = (Button) l.findViewById(R.id.btn_complete_save);
        final Button fbShare = (Button) l.findViewById(R.id.btn_share_facebook);
//        final Button share = (Button) l.findViewById(R.id.btn_complete_share);

        if(isLocalRecipe)
            save.setEnabled(false);

        back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(getActivity(), Central.class);
                startActivity(intent);
            }
        });

        save.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(!isLocalRecipe) {
                    DatabaseHelper db = new DatabaseHelper(getActivity().getApplicationContext());
                    Recipe_DB_MDL recipe = new Recipe_DB_MDL();
                    recipe.setTitle(recipeName);
                    recipe.addIngredients(ingr_list);
                    recipe.addSteps(step_list);

                    db.createRecipeEntry(recipe);

                    Recipe_DB_MDL lastRecipe = db.getLastRecipeEntry();
                    db.createIngredientEntries(ingr_list, lastRecipe);
                    db.createStepEntries(step_list, lastRecipe);
                    db.closeDB();
                    save.setEnabled(false);
                    Toast.makeText(getActivity().getApplicationContext(), "Recipe Saved", Toast.LENGTH_SHORT).show();
                } else { // Else will never be execute because button is hidden when else would occur
                    Toast.makeText(getActivity().getApplicationContext(), "Local Recipe", Toast.LENGTH_SHORT).show();
                }
            }
        });

        fbShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {


//                Bitmap bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.soup);
//                List<Bitmap> images = new ArrayList<Bitmap>();
//                images.add(bitmap);

                OpenGraphObject recipe = OpenGraphObject.Factory.createForPost("tastyratios:share");
                recipe.setProperty("title", recipeName);
                recipe.setProperty("image", "http://i.imgur.com/qwgF4xh.png");

                OpenGraphAction action = GraphObject.Factory.create(OpenGraphAction.class);
                action.setProperty("recipe", recipe);

                FacebookDialog shareDialog = new FacebookDialog.OpenGraphActionDialogBuilder(
                        getActivity(),
                        action,
                        "tastyratios:share",
                        "recipe"
                )
//                .setImageAttachmentsForAction(images, false)
                .build();
                uiHelper.trackPendingDialogCall(shareDialog.present());
            }
        });

        return l;
    }


    private class Tweet implements View.OnClickListener {
        @Override
        public void onClick(View v) {
            // TODO Auto-generated method stub
            tDialog = new Dialog(getActivity());
            tDialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
            tDialog.setContentView(R.layout.tweet_dialog);
            tweet_text = (EditText) tDialog.findViewById(R.id.tweet_text);
            tweet_text.setHint("#tastyratios");
            ImageView post_tweet = (ImageView) tDialog.findViewById(R.id.post_tweet);
            post_tweet.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    // TODO Auto-generated method stub
                    new PostTweet().execute();
                }
            });
            tDialog.show();
        }}
    private class PostTweet extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progress = new ProgressDialog(getActivity());
            progress.setMessage("Posting tweet ...");
            progress.setProgressStyle(ProgressDialog.STYLE_SPINNER);
            progress.setIndeterminate(true);
            tweetText = tweet_text.getText().toString();
            progress.show();
        }
        protected String doInBackground(String... args) {
            ConfigurationBuilder builder = new ConfigurationBuilder();
            builder.setOAuthConsumerKey(pref.getString("CONSUMER_KEY", ""));
            builder.setOAuthConsumerSecret(pref.getString("CONSUMER_SECRET", ""));
            AccessToken accessToken = new AccessToken(pref.getString("ACCESS_TOKEN", "")
                    , pref.getString("ACCESS_TOKEN_SECRET", ""));
            Twitter twitter = new TwitterFactory(builder.build()).getInstance(accessToken);

            BitmapFactory.Options opts = new BitmapFactory.Options();
            Resources food = getResources();
            Bitmap bm = BitmapFactory.decodeResource(food, R.drawable.soup, opts);
            File f = new File(getActivity().getCacheDir(), "newfile");
            try {
                f.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }
            FileOutputStream fos = null;
            try {
                fos = new FileOutputStream(f);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            bm.compress(Bitmap.CompressFormat.JPEG, 50, fos);
            try {
                StatusUpdate status = new StatusUpdate("#tastyratios "+recipeName + " " + tweetText);
                status.setMedia(f);
                twitter.updateStatus(status);
                return "";
            } catch (TwitterException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            finally{
                f.delete();
                try {
                    fos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            return null;
        }
        protected void onPostExecute(String res) {
            if(res != null){
                progress.dismiss();
                Toast.makeText(getActivity(), "Tweet Sucessfully Posted", Toast.LENGTH_SHORT).show();
                tDialog.dismiss();
            }else{
                progress.dismiss();
                Toast.makeText(getActivity(), "Error while tweeting!", Toast.LENGTH_SHORT).show();
                tDialog.dismiss();
            }
        }
    }


    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {

        public void onFragmentInteraction(Uri uri);
    }

    @Override
    public void onResume() {
        super.onResume();
        uiHelper.onResume();
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        uiHelper.onSaveInstanceState(outState);
    }

    @Override
    public void onPause() {
        super.onPause();
        uiHelper.onPause();
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        uiHelper.onDestroy();
    }

    //decodes image and scales it to reduce memory consumption
    private Bitmap decodeFile(File f){
        try {
            //decode image size
            BitmapFactory.Options o = new BitmapFactory.Options();
            o.inJustDecodeBounds = true;
            FileInputStream stream1=new FileInputStream(f);
            BitmapFactory.decodeStream(stream1,null,o);
            stream1.close();

            //Find the correct scale value. It should be the power of 2.
            final int REQUIRED_SIZE=70;
            int width_tmp=o.outWidth, height_tmp=o.outHeight;
            int scale=1;
            while(true){
                if(width_tmp/2<REQUIRED_SIZE || height_tmp/2<REQUIRED_SIZE)
                    break;
                width_tmp/=2;
                height_tmp/=2;
                scale*=2;
            }

            //decode with inSampleSize
            BitmapFactory.Options o2 = new BitmapFactory.Options();
            o2.inSampleSize=scale;
            FileInputStream stream2=new FileInputStream(f);
            Bitmap bitmap=BitmapFactory.decodeStream(stream2, null, o2);
            stream2.close();
            return bitmap;
        } catch (FileNotFoundException e) {
        }
        catch (IOException e) {
            e.printStackTrace();
        }
        return null;
    }



}
