package ctrl_alt_believe.tastyratios;

import org.jsoup.select.Elements;

/**
 * Created by Paolo on 05/11/2014.
 */
public interface WebParse_Base {
    abstract Elements getElementIngredients();
    abstract Elements getElementSteps();
    abstract Recipe_DB_MDL setDBRecipe();
    abstract Ingr_DB_MDL setDBIngredients();
    abstract Steps_DB_MDL setDBSteps();
}
