package ctrl_alt_believe.tastyratios;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;

/**
 * Created by sparqy on 07/10/14.
 */
public class Recipe_DB_MDL implements Parcelable {

    int RID;
    String title;
    String from;
    ArrayList<Ingr_DB_MDL> ingList;
    ArrayList<Steps_DB_MDL> stepsList;

    public String getFrom() { return from; }

    public void setFrom(String from) { this.from = from; }

    //Constructor
    public Recipe_DB_MDL(){
        ingList = new ArrayList<Ingr_DB_MDL>();
        stepsList = new ArrayList<Steps_DB_MDL>();
    }

    public Recipe_DB_MDL(String title){
        this.title = title;
        ingList = new ArrayList<Ingr_DB_MDL>();
        stepsList = new ArrayList<Steps_DB_MDL>();
    }

    public Recipe_DB_MDL(Parcel in){
        ingList = new ArrayList<Ingr_DB_MDL>();
        stepsList = new ArrayList<Steps_DB_MDL>();
        RID = in.readInt();
        title = in.readString();
        from = in.readString();
        in.readTypedList(ingList, Ingr_DB_MDL.CREATOR);
        in.readTypedList(stepsList, Steps_DB_MDL.CREATOR);
    }

    public void setTitle(String title){
        this.title = title;
    }

    public int getRID(){
        return this.RID;
    }

    public void setRID(int RID) { this.RID = RID; }

    public String toString() {
        return title;
    }

    public String getTitle(){
        return this.title;
    }

    public void addIngredients(ArrayList<Ingr_DB_MDL> ingredients) {
        ingList.addAll(ingredients);
    }

    public void addSteps(ArrayList<Steps_DB_MDL> steps) {
        stepsList.addAll(steps);
    }

    public ArrayList<Ingr_DB_MDL> getIngredients() {
        return ingList;
    }

    public ArrayList<Steps_DB_MDL> getSteps() {
        return stepsList;
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(RID);
        dest.writeString(title);
        dest.writeString(from);
        dest.writeTypedList(ingList);
        dest.writeTypedList(stepsList);
    }

    public static final Parcelable.Creator<Recipe_DB_MDL> CREATOR = new Parcelable.Creator<Recipe_DB_MDL>() {
        public Recipe_DB_MDL createFromParcel(Parcel in) {
            return new Recipe_DB_MDL(in);
        }

        public Recipe_DB_MDL[] newArray(int size) {
            return new Recipe_DB_MDL[size];
        }
    };
}
