package ctrl_alt_believe.tastyratios;

import android.app.ActionBar;
import android.app.AlertDialog;
import android.app.FragmentTransaction;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.SparseArray;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;


public class Create_Recipe extends FragmentActivity implements ActionBar.TabListener{

    ViewPager vp = null;

    ActionBar actionBar;

    ListView l;

    DatabaseHelper db;

    String recipeName;

    boolean named = false;

    final Context c = this;

    ArrayList<String> ingrAL;

    MyAdapter ma;

    static String ingr_name, ingr_unit, ingr_amnt;

    static Recipe_DB_MDL currentRecipe;

    TextView recipeNameIngrTab, recipeNameStepTab;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create__recipe);

        db = new DatabaseHelper(this);

        vp = (ViewPager) findViewById(R.id.create_recipe_pager);
        FragmentManager fm = getSupportFragmentManager();
        ma = new MyAdapter(fm);
        vp.setAdapter(ma);
        vp.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                //actionBar.setSelectedNavigationItem(position);
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

//        actionBar = getActionBar();
//        actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);
//
//        ActionBar.Tab ingrTab = actionBar.newTab();
//        ingrTab.setText("Ingredients");
//        ingrTab.setTabListener(this);
//
//        ActionBar.Tab stepTab = actionBar.newTab();
//        stepTab.setText("Steps");
//        stepTab.setTabListener(this);
//
//        actionBar.addTab(ingrTab);
//        actionBar.addTab(stepTab);

        recipeNameIngrTab = (TextView) findViewById(R.id.tvAdd_Ingr_ActionBar);
        recipeNameStepTab = (TextView) findViewById(R.id.tvAdd_Step_ActionBar);

        l = (ListView) findViewById(R.id.lv_add_ingr);

        enterRecipeName(c);
    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction ft) {

    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction ft) {

    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft) {
        vp.setCurrentItem(tab.getPosition());
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.create__recipe, menu);
        return true;
    }
    @Override
    protected  void onDestroy(){
        super.onDestroy();
        db.closeDB();
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void enterRecipeName(Context c){
        AlertDialog.Builder alert = new AlertDialog.Builder(this);

        final Context x = c;
        alert.setTitle("Recipe Name");
        alert.setMessage("Enter recipe Name");

        // Set an EditText view to get user input
        final EditText input = new EditText(this);
        input.setText("");
        alert.setView(input);

        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {

                if(input.getText().toString().equals("")){

                    Toast.makeText(x, "You must enter a name", Toast.LENGTH_SHORT).show();
                    named = false;
                    enterRecipeName(x);
                }else{

                    recipeName = input.getText().toString();
                    //setTitle(recipeName);
                    Add_Ingr_Fragment current = (Add_Ingr_Fragment) ma.getFragment(0);
                    Add_Steps_Fragment current1 = (Add_Steps_Fragment) ma.getFragment(1);
                    current.setTitle(recipeName);
                    current1.setTitle(recipeName);
                    named = true;
                    saveToDb();
                }


            }
        });

        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                dialog.cancel();
                finish();
            }
        });

        alert.show();

    }

    private Recipe_DB_MDL saveToDb(){
        Recipe_DB_MDL r = new Recipe_DB_MDL(recipeName);
        db.createRecipeEntry(r);
        currentRecipe = db.getLastRecipeEntry();

        return r;
    }


    public void addIngr(View v){

        // get prompts.xml view
        LayoutInflater li = LayoutInflater.from(this);
        final View promptsView = li.inflate(R.layout.activity_add_ingr_dialog, null);

        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(
                this);

        // set title / message
        alertDialogBuilder.setTitle("Add ingredient");

        final EditText et_ingrName = (EditText) promptsView
                .findViewById(R.id.etIngrName);

        final Spinner sp_ingrUnits = (Spinner) promptsView
                .findViewById(R.id.spUnits);

        ArrayAdapter<CharSequence> unitsAdapter = ArrayAdapter.createFromResource(this, R.array.units, android.R.layout.simple_spinner_item);
        unitsAdapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);

        // set prompts.xml to alertdialog builder
        alertDialogBuilder.setView(promptsView);

        sp_ingrUnits.setAdapter(unitsAdapter);

        final EditText et_ingrAmnt = (EditText) promptsView
                .findViewById(R.id.etIngrAmount);

        // set dialog message
        alertDialogBuilder
                .setCancelable(true)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                // get user input and set it to result
                                // edit text
                                ingr_name = et_ingrName.getText().toString();
                                ingr_unit = sp_ingrUnits.getSelectedItem().toString();
                                ingr_amnt = et_ingrAmnt.getText().toString();

                                addToList(ingr_name, ingr_amnt, ingr_unit);
                            }
                        });

        // create alert dialog
        AlertDialog alertDialog = alertDialogBuilder.create();

        // show it
        alertDialog.show();

    }

    private void addToList(String name, String amnt, String unit){
        Add_Ingr_Fragment current = (Add_Ingr_Fragment) ma.getFragment(0);

        current.updateList(name, amnt, unit);
    }

    public void saveRecipe(View v){
        Add_Ingr_Fragment current = (Add_Ingr_Fragment) ma.getFragment(0);

        boolean p = db.createIngredientEntries(current.getIngrMdlArr(), currentRecipe);

        if (p){
            Toast.makeText(c, "SAVED", Toast.LENGTH_SHORT).show();
        }
    }

    public void addStep(View v){
        AlertDialog.Builder alert = new AlertDialog.Builder(this);

        final Context x = c;
        alert.setTitle("Enter");
        alert.setMessage("Recipe step instructions");

        // Set an EditText view to get user input
        final EditText input = new EditText(this);
        alert.setView(input);

        alert.setPositiveButton("Ok", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                Add_Steps_Fragment current = (Add_Steps_Fragment) ma.getFragment(1);

                current.createStepCard(input.getText().toString(), currentRecipe);
            }
        });

        alert.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {

            }
        });

        alert.show();

    }

    public void saveRecipeSteps(View v){
        Add_Steps_Fragment current = (Add_Steps_Fragment) ma.getFragment(1);

        boolean p = db.createStepEntries(current.getStepArr(), currentRecipe);

        if (p){
            Toast.makeText(c, "SAVED", Toast.LENGTH_SHORT).show();
        }
    }

    public void measureIngr(View v)
    {
        Intent i = new Intent("android.intent.action.MEASURE_INGR");

        startActivity(i);
    }

}

class MyAdapter extends FragmentStatePagerAdapter {

    SparseArray<Fragment> registeredFragments = new SparseArray<Fragment>();

    Fragment fragment=null;

    public MyAdapter(FragmentManager fm){
        super(fm);
    }
    @Override
    public Fragment getItem(int i) {
        if(i==0){
            fragment = new Add_Ingr_Fragment();
            registeredFragments.put(i, fragment);
        }else{
            fragment = new Add_Steps_Fragment();
            registeredFragments.put(i, fragment);
        }
        return fragment;
    }

    @Override
    public int getCount() {
        return 2;
    }

    public Fragment getFragment(int position){
        return registeredFragments.get(position);
    }

}

