package ctrl_alt_believe.tastyratios;

import java.util.ArrayList;

/**
 * Created by Paolo on 27/10/2014.
 */
public interface WebSearch_Base {
    abstract String getURL();
    abstract ArrayList<WebRecipeResult> search();
}
