package ctrl_alt_believe.tastyratios;

import android.content.Context;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import it.gmariotti.cardslib.library.internal.Card;

/**
 * Created by sparqy on 09/10/14.
 */
public class ingr_card extends Card {

    protected TextView amnt;

    public ingr_card(Context context){
        this(context, R.layout.ingr_card_inner);
    }

    public ingr_card(Context context, int innerLayout){
        super(context, innerLayout);
        init();
    }

    private void init(){
        setOnClickListener(new OnCardClickListener() {
            @Override
            public void onClick(Card card, View view) {
                //Toast.makeText(getContext(), "Click Listener card=", Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void setupInnerViewElements(ViewGroup parent, View view) {

        //Retrieve elements
        amnt = (TextView) parent.findViewById(R.id.tv_ingr_amnt_inner);

    }

}
