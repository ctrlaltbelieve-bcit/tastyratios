package ctrl_alt_believe.tastyratios;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;
import android.net.Uri;
import android.support.v4.app.FragmentActivity;



public class Register extends Activity {
    private EditText fullName = null;
    private EditText regEmail = null;
    private EditText regPass = null;

//    private FBFragment fbFragment;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // Set View to register.xml
        setContentView(R.layout.activity_register);

        EditText edit_txt = (EditText) findViewById(R.id.reg_password);

        edit_txt.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId,
                                          KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    Submit(findViewById(R.id.reg_password));
                    return true;
                }
                return false;
            }
        });

        TextView loginScreen = (TextView) findViewById(R.id.link_to_login);

        // Listening to Login Screen link
        loginScreen.setOnClickListener(new View.OnClickListener() {

            public void onClick(View arg0) {
                // Closing registration screen
                // Switching to Login Screen/closing register screen
                finish();
            }
        });
    }

//    @Override
//    public void onFragmentInteraction(Uri uri) {
//
//    }

    public void Submit (View view){
        fullName = (EditText)findViewById(R.id.reg_fullname);
        regEmail = (EditText)findViewById(R.id.reg_email);
        regPass = (EditText)findViewById(R.id.reg_password);
        String name = fullName.getText().toString();
        String email = regEmail.getText().toString();
        String pass = regPass.getText().toString();

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        SharedPreferences.Editor editor = preferences.edit();

        if(name.equals("")){
            Toast.makeText(getApplicationContext(), "Full Name field is blank",
                    Toast.LENGTH_SHORT).show();
        }
        else if (email.equals("")){
            Toast.makeText(getApplicationContext(), "Email field is blank",
                    Toast.LENGTH_SHORT).show();
        }
        else if (pass.equals("")){
            Toast.makeText(getApplicationContext(), "Password field is blank",
                    Toast.LENGTH_SHORT).show();
        }
        else {

            editor.putString("Full_Name", name);
            editor.putString("Email", email);
            editor.putString("Password", pass);
            editor.apply();
            Toast.makeText(getApplicationContext(), preferences.getString("Full_Name", name)+ " Registered",
                    Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(getApplicationContext(), Login2.class);
            startActivity(intent);


        }

    }
}