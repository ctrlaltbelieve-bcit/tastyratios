package ctrl_alt_believe.tastyratios;

import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.text.method.ScrollingMovementMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;


public class StepFragment extends Fragment {

    private ArrayList<Steps_DB_MDL> step_list;
    private ArrayList<Ingr_DB_MDL> ingredientList;

    private int position;

    private ImageView imageView;
    private OnFragmentInteractionListener mListener;

    private TextView stepNumber, recipeName;

    public StepFragment() {}

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        Button measureIngredientBtn;
        String name = "Tasty Ratios";

        if (getArguments() != null) {
            final Bundle args = getArguments();
            step_list = args.getParcelableArrayList("Steps");
            ingredientList = getArguments().getParcelableArrayList("Ingredients");
            position = args.getInt("Position");
            name = args.getString("RecipeName");

        }

        LinearLayout l = (LinearLayout) inflater.inflate(R.layout.fragment_step, container, false);
        TextView tv = (TextView) l.findViewById(R.id.step_text);
        measureIngredientBtn = (Button) l.findViewById(R.id.measure_ingredients);

        TextView text = (TextView) l.findViewById(R.id.step_text);
        text.setMovementMethod(new ScrollingMovementMethod());

        tv.setText(step_list.get(position).getInstructions());
        measureIngredientBtn.setOnClickListener(new MeasureIngredientsListener());

        imageView = (ImageView) l.findViewById(R.id.ivStepImage);
        imageView.setImageResource(R.drawable.soup);

        stepNumber = (TextView) l.findViewById(R.id.tvStepsNum_ActionBar);
        stepNumber.setText("Step " + (position + 1) + " /" + step_list.size( ));

        recipeName = (TextView) l.findViewById(R.id.tvSteps_ActionBar);

        recipeName.setText(name);
        // Inflate the layout for this fragment
        return l;
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {
        public void onFragmentInteraction(Uri uri);
    }

    public class MeasureIngredientsListener implements View.OnClickListener {
        @Override
        public void onClick(final View v) {
            CharSequence[] ingredientMeasure = new CharSequence[ingredientList.size()];
            for (int i = 0; i < ingredientList.size(); i++) {
                ingredientMeasure[i] = ingredientList.get(i).getName();
            }
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setTitle("Measure ingredient");
            builder.setItems(ingredientMeasure, new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    Intent intent = new Intent(v.getContext(), MeasureIngr.class);
                    intent.putExtra("Ingredient", ingredientList.get(which));
                    startActivity(intent);
                }
            });
            builder.show();
        }
    }

}
