package ctrl_alt_believe.tastyratios;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;

public class Add_Ingr_Fragment extends Fragment {

    ListView l;

    ArrayList<String> ingrAL = new ArrayList<String>();
    ArrayList<Ingr_DB_MDL> ingr_db_mdls = new ArrayList<Ingr_DB_MDL>();
    ArrayAdapter<String> lv_arr_adapter;
    View rootView;
    TextView actionBar;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        rootView = inflater.inflate(R.layout.fragment_add__ingr, container, false);
        l = (ListView) rootView.findViewById(R.id.lv_add_ingr);

        lv_arr_adapter = new ArrayAdapter<String>(getActivity(), R.layout.add_ingr_list_item , ingrAL );

        l.setAdapter(lv_arr_adapter);

        actionBar = (TextView) rootView.findViewById(R.id.tvAdd_Ingr_ActionBar);

        Log.e("Add Ingr Fragment", "Calling onCreate");

        return rootView;


    }


    public void updateList(String name, String amnt, String unit){

        Ingr_DB_MDL newIngrMdl = new Ingr_DB_MDL();

        newIngrMdl.setName(name);
        newIngrMdl.setAmount(amnt);
        newIngrMdl.setUnit(unit);

        ingr_db_mdls.add(newIngrMdl);

        ingrAL.add(name + "     " + amnt + "   " + unit);

        lv_arr_adapter = new ArrayAdapter<String>(getActivity(), R.layout.add_ingr_list_item, ingrAL );

        l.setAdapter(lv_arr_adapter);
    }

    public void setTitle( String recipeName )
    {
        actionBar.setText(recipeName);
    }

    public ArrayList<Ingr_DB_MDL> getIngrMdlArr(){
        return this.ingr_db_mdls;
    }
}
