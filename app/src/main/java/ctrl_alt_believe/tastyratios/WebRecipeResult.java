package ctrl_alt_believe.tastyratios;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Paolo on 10/27/2014.
 */
public class WebRecipeResult implements Parcelable {
    String recipeName,
           recipeLink;

    public WebRecipeResult() {
        super();
    }

    public WebRecipeResult(Parcel in) {
        recipeName = in.readString();
        recipeLink = in.readString();
    }

    public WebRecipeResult(String recipeName, String recipeLink) {
        this.recipeName = recipeName;
        this.recipeLink = recipeLink;
    }

    public void setRecipeName(String recipeName) {
        this.recipeName = recipeName;
    }

    public void setRecipeLink(String recipeLink) {
        this.recipeLink = recipeLink;
    }

    public String getRecipeName() {
        return recipeName;
    }

    public String getRecipeLink() {
        return recipeLink;
    }

    @Override
    public String toString() {
        return recipeName;
    }

    public static final Parcelable.Creator<WebRecipeResult> CREATOR = new ClassLoaderCreator<WebRecipeResult>() {

        @Override
        public WebRecipeResult createFromParcel(Parcel source, ClassLoader loader) {
            return new WebRecipeResult(source);
        }

        @Override
        public WebRecipeResult createFromParcel(Parcel source) {
            return null;
        }

        @Override
        public WebRecipeResult[] newArray(int size) {
            return new WebRecipeResult[0];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(recipeName);
        dest.writeString(recipeLink);

    }
}
