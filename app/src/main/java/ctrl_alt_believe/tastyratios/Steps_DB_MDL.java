package ctrl_alt_believe.tastyratios;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by sparqy on 23/10/14.
 */
public class Steps_DB_MDL implements Parcelable {

    int RID, FID, delta;
    String instructions;

    public Steps_DB_MDL(){}

    public Steps_DB_MDL(Parcel in){
        RID = in.readInt();
        FID = in.readInt();
        delta = in.readInt();
        instructions = in.readString();
    }

    public Steps_DB_MDL(int delta, String instructions) {
        this.delta = delta;
        this.instructions = instructions;
    }

    public Steps_DB_MDL(int rid, int delta, String instructions){
        this.RID = rid;
        this.delta = delta;
        this.instructions = instructions;
    }

    public int getRID() {
        return RID;
    }

    public void setRID(int RID) {
        this.RID = RID;
    }

    public int getFID() {
        return FID;
    }

    public void setFID(int FID) {
        this.FID = FID;
    }

    public int getDelta() {
        return delta;
    }

    public void setDelta(int delta) {
        this.delta = delta;
    }

    public String getInstructions() {
        return instructions;
    }

    public String toString() {
        return instructions;
    }

    public void setInstructions(String instructions) {
        this.instructions = instructions;
    }

    public static final Parcelable.Creator<Steps_DB_MDL> CREATOR = new Parcelable.Creator<Steps_DB_MDL>() {
        public Steps_DB_MDL createFromParcel(Parcel in) {
            return new Steps_DB_MDL(in);
        }

        public Steps_DB_MDL[] newArray(int size) {
            return new Steps_DB_MDL[size];
        }
    };

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(RID);
        dest.writeInt(FID);
        dest.writeInt(delta);
        dest.writeString(instructions);
    }
}
