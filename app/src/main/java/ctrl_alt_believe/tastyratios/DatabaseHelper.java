package ctrl_alt_believe.tastyratios;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by sparqy on 07/10/14.
 */
public class DatabaseHelper extends SQLiteOpenHelper {

    //Logcat tag
    private static final String LOG = "DatabaseHelper";

    //Database Version
    private static final int DATABASE_VERSION = 1;

    //Database Name
    private static final String DATABASE_NAME = "TastyRatios";

    //Table Names
    private static final String TABLE_RECIPE = "recipe";
    private static final String TABLE_INGREDIENTS = "ingredients";
    private static final String TABLE_STEPS = "steps";
    private static final String TABLE_FILES = "files";

    //Recipe Table - column
    private static final String KEY_RID = "_id";
    private static final String KEY_TITLE = "title";
    private static final String KEY_FROM = "from";

    //Ingredients Table - columns
    private static final String KEY_IID = "_id";
    private static final String KEY_INGR_RID = "rid";
    private static final String KEY_INGR_FID = "fid";
    private static final String KEY_INGR_NAME = "ingr_name";
    private static final String KEY_AMOUNT = "amount";
    private static final String KEY_UNIT = "unit";

    //Steps Table - columns
    private static final String KEY_SID = "_id";
    private static final String KEY_DELTA = "delta";
    private static final String KEY_INSTRUCTIONS = "instructions";

    //Files Table - columns
    private static final String KEY_FID = "_id";
    private static final String KEY_URI = "uri";

    // Table Create Statements
    // Recipe table create statement
    private static final String CREATE_TABLE_RECIPE = "CREATE TABLE "
            + TABLE_RECIPE + "(" + KEY_RID + " INTEGER PRIMARY KEY,"
            + KEY_TITLE + " TEXT"
            + ")";

    //Ingredients table create statement
    private static final String CREATE_TABLE_INGREDIENTS = "CREATE TABLE "
            + TABLE_INGREDIENTS + "(" + KEY_IID + " INTEGER,"
            + KEY_INGR_RID + " INTEGER,"
            + KEY_INGR_FID + " INTEGER,"
            + KEY_INGR_NAME + " TEXT,"
            + KEY_AMOUNT + " TEXT,"
            + KEY_UNIT + " TEXT,"
            + " PRIMARY KEY (" + KEY_IID + "," + KEY_INGR_RID + "),"
            + "FOREIGN KEY(" + KEY_INGR_RID + ") REFERENCES " + TABLE_RECIPE + "(" + KEY_RID + ")"
            + ")";

    //Steps table create statement
    private static final String CREATE_TABLE_STEPS = "CREATE TABLE "
            + TABLE_STEPS + "(" + KEY_SID + " INTEGER,"
            + KEY_INGR_RID + " INTEGER,"
            + KEY_DELTA + " INTEGER,"
            + KEY_INSTRUCTIONS + " TEXT,"
            + KEY_INGR_FID + " INTEGER,"
            + "PRIMARY KEY (" +KEY_SID + "," +KEY_INGR_RID + ", " + KEY_DELTA + " ),"
            + "FOREIGN KEY(" + KEY_INGR_RID + ") REFERENCES " + TABLE_RECIPE + "(" + KEY_RID + ")"
            +")";

    // table create statement
    private static final String CREATE_TABLE_FILES= "CREATE TABLE "
            + TABLE_FILES + "(" + KEY_FID + " INTEGER PRIMARY KEY,"
            + KEY_URI + " TEXT"
            + ")";


    public DatabaseHelper (Context context){
        super (context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db){

        // creating required tables
        db.execSQL(CREATE_TABLE_RECIPE);
        db.execSQL(CREATE_TABLE_INGREDIENTS);
        db.execSQL(CREATE_TABLE_STEPS);
        db.execSQL(CREATE_TABLE_FILES);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){
        // on upgrade drop older tables
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_RECIPE);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_INGREDIENTS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_STEPS);
        db.execSQL("DROP TABLE IF EXISTS " + TABLE_FILES);

        // create new tables
        onCreate(db);
    }

    public void createRecipeEntry (Recipe_DB_MDL recipe){

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_TITLE, recipe.getTitle());

        db.insert(TABLE_RECIPE, null, values);

    }

    public void deleteRecipeEntry(Recipe_DB_MDL recipe) {
        SQLiteDatabase db = this.getWritableDatabase();

        int recipeID = recipe.getRID();

        String delete = "DELETE FROM " + TABLE_RECIPE + " WHERE " + KEY_RID + " = " + recipeID;

        db.execSQL(delete);

    }

    public void createIngredientEntry ( Ingr_DB_MDL ingr, Recipe_DB_MDL r){

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_INGR_RID, r.getRID());
        values.put(KEY_INGR_NAME, ingr.getName());
        values.put(KEY_AMOUNT, ingr.getAmount());
        values.put(KEY_UNIT, ingr.getUnit());

        db.insert(TABLE_INGREDIENTS, null, values);
    }

    public boolean createIngredientEntries ( ArrayList<Ingr_DB_MDL> ingrMdls, Recipe_DB_MDL r  ){

        boolean p = false;

        for (Ingr_DB_MDL x: ingrMdls){

            createIngredientEntry(x, r);
        }
        p = true;

        return p;
    }

    public void createStepsEntry ( Steps_DB_MDL s, Recipe_DB_MDL r){

        SQLiteDatabase db = this.getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put(KEY_INGR_RID, r.getRID());
        values.put(KEY_DELTA, s.getDelta());
        values.put(KEY_INSTRUCTIONS, s.getInstructions());

        db.insert(TABLE_STEPS, null, values);
    }

    public boolean createStepEntries ( ArrayList<Steps_DB_MDL> stepMdls, Recipe_DB_MDL r  ){

        boolean p = false;

        for (Steps_DB_MDL x: stepMdls){

            createStepsEntry(x, r);
        }
        p = true;

        return p;
    }

    public ArrayList<Ingr_DB_MDL> getRecipeIngr ( Recipe_DB_MDL r){

        SQLiteDatabase db = this.getWritableDatabase();

        int RID = r.getRID();

        String selectQuery = "SELECT  * FROM " + TABLE_INGREDIENTS + " WHERE "
                + KEY_INGR_RID + " = " + RID;

        Log.i(LOG, selectQuery);

        Cursor c = db.rawQuery(selectQuery, null);

        ArrayList<Ingr_DB_MDL> i = new ArrayList<Ingr_DB_MDL>();

        if (c != null){

            while (c.moveToNext()) {
                Ingr_DB_MDL current = new Ingr_DB_MDL();
                current.setIID(c.getColumnIndex(KEY_IID));
                current.setName((c.getString(c.getColumnIndex(KEY_INGR_NAME))));
                current.setAmount(c.getString(c.getColumnIndex(KEY_AMOUNT)));
                current.setUnit(c.getString(c.getColumnIndex(KEY_UNIT)));
                i.add(current);
            }
        }

        return i;
    }

    public ArrayList<Steps_DB_MDL> getRecipeSteps ( Recipe_DB_MDL r){

        SQLiteDatabase db = this.getWritableDatabase();

        long RID = r.getRID();

        String selectQuery = "SELECT  * FROM " + TABLE_STEPS + " WHERE "
                + KEY_INGR_RID + " = " + RID;

        Log.i(LOG, selectQuery);

        Cursor c = db.rawQuery(selectQuery, null);

        ArrayList<Steps_DB_MDL> i = new ArrayList<Steps_DB_MDL>();

        if (c != null){

            while (c.moveToNext()) {
                Steps_DB_MDL current = new Steps_DB_MDL();
                current.setRID(c.getColumnIndex(KEY_RID));
                current.setInstructions((c.getString(c.getColumnIndex(KEY_INSTRUCTIONS))));
                current.setDelta(c.getInt(c.getColumnIndex(KEY_DELTA)));
                current.setFID(c.getInt(c.getColumnIndex(KEY_FID)));
                i.add(current);
                Log.i(LOG, "" + c.getString(c.getColumnIndex(KEY_INSTRUCTIONS)));
            }
        }

        return i;
    }

    public Recipe_DB_MDL getRecipe(int RID) {
        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "SELECT  * FROM " + TABLE_RECIPE + " WHERE "
                + KEY_RID + " = " + RID;

        Log.i(LOG, selectQuery);

        Cursor c = db.rawQuery(selectQuery, null);

        if (c != null)
            c.moveToFirst();

        Recipe_DB_MDL r = new Recipe_DB_MDL();
        r.setRID(c.getInt(c.getColumnIndex(KEY_RID)));
        r.setTitle((c.getString(c.getColumnIndex(KEY_TITLE))));

        return r;
    }

    public ArrayList<Recipe_DB_MDL> getAllRecipes() {
        SQLiteDatabase db = this.getReadableDatabase();
        Recipe_DB_MDL recipe;

        ArrayList<Recipe_DB_MDL> list = new ArrayList<Recipe_DB_MDL>();

        String select = "SELECT * FROM " + TABLE_RECIPE;

        Cursor c = db.rawQuery(select, null);
        try {
            while (c.moveToNext()) {
                recipe = new Recipe_DB_MDL();
                recipe.setRID(c.getInt(c.getColumnIndex(KEY_RID)));
                recipe.setTitle(c.getString(c.getColumnIndex(KEY_TITLE)));
//                recipe.setFrom(c.getString(c.getColumnIndex(KEY_FROM)));
                list.add(recipe);
            }
        } catch (Exception e) {
            System.out.println("Error: " + e);
        }

        return list;
    }

    public void closeDB() {
        SQLiteDatabase db = this.getReadableDatabase();
        if (db != null && db.isOpen())
            db.close();
    }

    public Recipe_DB_MDL getLastRecipeEntry( ){
        SQLiteDatabase db = this.getReadableDatabase();

        String selectQuery = "SELECT  * FROM " + TABLE_RECIPE + " ORDER BY "
                + KEY_RID + " DESC LIMIT 1";

        Cursor c = db.rawQuery(selectQuery, null);

        if (c != null)
            c.moveToFirst();

        Recipe_DB_MDL r = new Recipe_DB_MDL();
        r.setRID(c.getInt(c.getColumnIndex(KEY_RID)));
        r.setTitle((c.getString(c.getColumnIndex(KEY_TITLE))));

        return r;
    }

    public void clearRecipes() {
        SQLiteDatabase db = this.getWritableDatabase();
        db.delete(TABLE_RECIPE, null, null);
        System.out.println("All recipes deleted from " + TABLE_RECIPE);
    }
}

