package ctrl_alt_believe.tastyratios;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;


public class AllRecipes extends Activity {

    DatabaseHelper db;

    ListView list;
    ArrayList<Recipe_DB_MDL> recipes;
    ArrayAdapter<Recipe_DB_MDL> adapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_recipes);

        db = new DatabaseHelper(getApplicationContext());

        recipes = db.getAllRecipes();

        adapter = new ArrayAdapter<Recipe_DB_MDL>(this, R.layout.recipe_list_item, recipes);

        list = (ListView) findViewById(R.id.recipe_list);
        list.setClickable(true);
        list.setAdapter(adapter);
        registerForContextMenu(list);
        list.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent;
                intent = new Intent(getApplicationContext(), FollowRecipe.class);
                Recipe_DB_MDL selection = recipes.get(position);

                selection.addSteps(db.getRecipeSteps(selection));
                selection.addIngredients(db.getRecipeIngr(selection));

                intent.putExtra("Recipe", selection);
                intent.putExtra("Local", true);
                startActivity(intent);
            }
        });
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v,
                                    ContextMenu.ContextMenuInfo menuInfo) {
        if (v.getId() == R.id.recipe_list) {
            String[] menuItems = getResources().getStringArray(R.array.recipe_context_menu);
            for (int i = 0; i<menuItems.length; i++) {
                menu.add(Menu.NONE, i, i, menuItems[i]);
            }
        }
    }

    @Override
    public boolean onContextItemSelected(MenuItem item) {
        db = new DatabaseHelper(getApplicationContext());
        AdapterView.AdapterContextMenuInfo info = (AdapterView.AdapterContextMenuInfo)item.getMenuInfo();
        int menuItemIndex = item.getItemId();
        String[] menuItems = getResources().getStringArray(R.array.recipe_context_menu);
        String menuItemName = menuItems[menuItemIndex];

        if(menuItemIndex == 0) {
            Toast.makeText(getApplicationContext(), "Recipe Deleted", Toast.LENGTH_SHORT).show();

            db.deleteRecipeEntry(recipes.get(info.position));
            recipes.remove(info.position);

            adapter.notifyDataSetChanged();
        }

        return super.onContextItemSelected(item);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.all_recipes, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }
}
