package ctrl_alt_believe.tastyratios;

import android.app.ActionBar;
import android.app.FragmentTransaction;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.Menu;
import android.view.MenuItem;

import java.util.ArrayList;

public class FollowRecipe extends FragmentActivity implements
        ActionBar.TabListener,
        StepFragment.OnFragmentInteractionListener,
        IngredientFragment.OnFragmentInteractionListener,
        FollowComplete.OnFragmentInteractionListener

{

    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v13.app.FragmentStatePagerAdapter}.
     */
    SectionsPagerAdapter mSectionsPagerAdapter;

    /**
     * The {@link ViewPager} that will host the section contents.
     */
    ViewPager mViewPager;

    private ActionBar actionBar;

    private int numFragments;
    private ArrayList<String> tabs;
    private ArrayList<Ingr_DB_MDL> ingredients;
    private ArrayList<Steps_DB_MDL> steps;
    private boolean local = false;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_follow_recipe);

        Intent in = getIntent();

        try { // If it's not a local variable then catch the null exception that will be thrown
            local = in.getExtras().getBoolean("Local");
        } catch (Exception e) {System.out.println("External recipe");}

        Recipe_DB_MDL recipe = (Recipe_DB_MDL) in.getParcelableExtra("Recipe");
        System.out.println(recipe.toString());

        ingredients = new ArrayList<Ingr_DB_MDL>();
        steps = new ArrayList<Steps_DB_MDL>();
        ingredients.addAll(recipe.getIngredients());
        steps.addAll(recipe.getSteps());
        tabs = new ArrayList<String>();

        tabs.add("Ingredients");
        for (int i = 1; i <= steps.size(); i++) {
            tabs.add("Step " + i);
        }
        tabs.add("Complete");

        Bundle bundle = new Bundle();
        bundle.putParcelableArrayList("Ingredients", ingredients);
        bundle.putParcelableArrayList("Steps", steps);
        bundle.putString("Title", recipe.getTitle());
        bundle.putBoolean("Local", local);

        String activityTitle = recipe.getTitle();
        setTitle(activityTitle);

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        FragmentManager fm = getSupportFragmentManager();
        mSectionsPagerAdapter = new SectionsPagerAdapter(fm, steps.size() + 2, bundle);

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.follow_recipe_pager);
        mViewPager.setAdapter(mSectionsPagerAdapter);

        actionBar = getActionBar();
        //actionBar.setHomeButtonEnabled(false);
        //actionBar.setNavigationMode(ActionBar.NAVIGATION_MODE_TABS);

        for (String tab_name : tabs) {
            //actionBar.addTab(actionBar.newTab().setText(tab_name).setTabListener(this));
        }

        mViewPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {

            @Override
            public void onPageSelected(int position) {
                // on changing the page
                // make respected tab selected
                //actionBar.setSelectedNavigationItem(position);
            }

            @Override
            public void onPageScrolled(int arg0, float arg1, int arg2) {
            }

            @Override
            public void onPageScrollStateChanged(int arg0) {
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.follow_recipe, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onFragmentInteraction(Uri uri) {

    }

    @Override
    public void onTabSelected(ActionBar.Tab tab, FragmentTransaction ft) {
        mViewPager.setCurrentItem(tab.getPosition());
    }

    @Override
    public void onTabUnselected(ActionBar.Tab tab, FragmentTransaction ft) {

    }

    @Override
    public void onTabReselected(ActionBar.Tab tab, FragmentTransaction ft) {

    }




    /**
     * A {@link FragmentPagerAdapter} that returns a fragment corresponding to
     * one of the sections/tabs/pages.
     */
    public class SectionsPagerAdapter extends FragmentStatePagerAdapter {

        private int fragments;
        private ArrayList<Steps_DB_MDL> step_list;
        private ArrayList<Ingr_DB_MDL> ingredient_list;
        private Bundle b;
        private String rName;

        public SectionsPagerAdapter(FragmentManager fm) {
            super(fm);
        }

        public SectionsPagerAdapter(FragmentManager fm, int fragments, Bundle b) {
            super(fm);
            this.fragments = fragments;
            step_list = b.getParcelableArrayList("Steps");
            ingredient_list = b.getParcelableArrayList("Ingredients");
            this.b = b;
            rName = b.getString("Title");
        }

        @Override
        public Fragment getItem(int position) {
        System.out.println("Fragment " + position + " of " + fragments);
            if(position == 0) {
                final IngredientFragment ingFragment = new IngredientFragment();
                ingFragment.setArguments(this.b); // Just pass the bundle along
                return ingFragment;
            } else if(position >= 1 && position < (fragments-1)) { // Can't pass existing bundle for this because it needs the position
                final StepFragment stepFragment = new StepFragment();
                Bundle bundleForSteps = new Bundle();
                bundleForSteps.putParcelableArrayList("Steps", step_list);
                bundleForSteps.putParcelableArrayList("Ingredients", ingredient_list);
                bundleForSteps.putInt("Position", position-1); // -1 because the first is ingredient fragment. This is working
                bundleForSteps.putString("RecipeName", b.getString("Title"));
                stepFragment.setArguments(bundleForSteps);
                return stepFragment;
            } else if(position < fragments) {
                final FollowComplete finalComplete = new FollowComplete();
                finalComplete.setArguments(this.b);
                return finalComplete;
            }
            System.out.println("Null fragment");
            return null;
        }

        @Override
        public int getCount() {
            return fragments;
        }


    }

}