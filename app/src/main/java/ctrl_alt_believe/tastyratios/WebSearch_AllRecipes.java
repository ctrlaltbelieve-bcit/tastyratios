package ctrl_alt_believe.tastyratios;

import android.net.Uri;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;

/**
 * Created by Paolo on 10/15/2014.
 */
public class WebSearch_AllRecipes implements WebSearch_Base {

    static String baseURL     = "http://allrecipes.com/";
    static String searchURL   = "search/default.aspx";
    static String searchParam = "?wt=";
    static String targetTags  = "a[class=title]";
    static String hrefTag     = "href";
    String query;

    public WebSearch_AllRecipes(String query) {
        this.query = query;
    }

    public String getURL() {
        return baseURL + searchURL + searchParam + Uri.encode(query);
    }

    public ArrayList<WebRecipeResult> search() {
        String url;
        ArrayList<WebRecipeResult> list;

        list = new ArrayList<WebRecipeResult>();
        url = baseURL + searchURL + searchParam + Uri.encode(query);

        try {
            Document document = Jsoup.connect(url).get();
            Elements results = document.select(targetTags);
            for (Element element: results) {
                String recipeName;
                String recipeLink;
                recipeName = Jsoup.parse(element.toString()).text();
                recipeLink = baseURL + element.attr(hrefTag);
                WebRecipeResult result = new WebRecipeResult(recipeName, recipeLink);
                list.add(result);
            }
        }
        catch (Exception e) {
            e.printStackTrace();
        }

        return list;
    }
}
