package ctrl_alt_believe.tastyratios;

import java.util.ArrayList;

/**
 * Created by Paolo on 16/10/2014.
 */
public class WebSearch_Food implements WebSearch_Base {

    static String baseURL = "http://food.com/";
    String query;
    String rawResults;

    public WebSearch_Food(String query) {
        this.query = query;
    }

    public String getURL() {
        return "";
    }
    public ArrayList<WebRecipeResult> search() {
        return new ArrayList<WebRecipeResult>();
    }
}
