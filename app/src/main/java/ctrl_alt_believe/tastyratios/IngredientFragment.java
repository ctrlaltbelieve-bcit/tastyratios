package ctrl_alt_believe.tastyratios;

import android.app.Activity;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;


public class IngredientFragment extends Fragment {

    private ArrayList<Ingr_DB_MDL> ingr_list;

    private OnFragmentInteractionListener mListener;

    public IngredientFragment() {

    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        if (getArguments() != null) {
            final Bundle args = getArguments();
            ingr_list = args.getParcelableArrayList("Ingredients");
        }

        LinearLayout l = (LinearLayout) inflater.inflate(R.layout.fragment_ingredient_list, container, false);

        IngredientListAdapter adapter;
        adapter = new IngredientListAdapter(this.getActivity(), R.layout.ingredient_list_item, ingr_list);

        ListView list = (ListView) l.findViewById(R.id.ingredient_list);

        list.setClickable(true);

        list.setAdapter(adapter);

        list.setOnItemClickListener(new ListView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Ingr_DB_MDL selection = ingr_list.get(position);
            }
        });

        Toast.makeText(l.getContext(), "Swipe -> for steps", Toast.LENGTH_SHORT).show();

        // Inflate the layout for this fragment
        return l;
        // inflater.inflate(R.layout.fragment_ingredient_list, container, false);
    }

    public void onButtonPressed(Uri uri) {
        if (mListener != null) {
            mListener.onFragmentInteraction(uri);
        }
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            mListener = (OnFragmentInteractionListener) activity;
        } catch (ClassCastException e) {
            throw new ClassCastException(activity.toString()
                    + " must implement OnFragmentInteractionListener");
        }
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    public interface OnFragmentInteractionListener {

        public void onFragmentInteraction(Uri uri);
    }

}
