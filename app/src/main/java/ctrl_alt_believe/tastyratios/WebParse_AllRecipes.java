package ctrl_alt_believe.tastyratios;

import android.util.Log;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.select.Elements;

/**
 * Created by Paolo on 11/9/2014.
 */
public class WebParse_AllRecipes implements WebParse_Base {
    static String selectIngredient = "p[class=fl-ing]";
    static String selectSteps = "span.plaincharacterwrap";
    String recipeURL;
    Document document;
    Elements ingredients,
             steps;

    public WebParse_AllRecipes(String url) {
        recipeURL = url;
        try {
            document = Jsoup.connect(recipeURL).get();
            ingredients = document.select(selectIngredient);
            steps = document.select(selectSteps);
        }
        catch (Exception e) {
            Log.e("Parser", "Something went wrong with this.");
            e.printStackTrace();
        }
    }

    @Override
    public Elements getElementIngredients() {
        return ingredients;
    }

    @Override
    public Elements getElementSteps() {
        return steps;
    }

    @Override
    public Recipe_DB_MDL setDBRecipe() {
        return null;
    }

    @Override
    public Ingr_DB_MDL setDBIngredients() {
        return null;
    }

    @Override
    public Steps_DB_MDL setDBSteps() {
        return null;
    }
}
