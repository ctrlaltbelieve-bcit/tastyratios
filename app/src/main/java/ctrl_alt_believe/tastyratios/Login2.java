package ctrl_alt_believe.tastyratios;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Color;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.view.KeyEvent;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


public class Login2 extends Activity {

    private EditText logEmail = null;
    private EditText logPass = null;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        // setting default screen to login.xml
        setContentView(R.layout.activity_login2);

        TextView registerScreen = (TextView) findViewById(R.id.link_to_register);
        EditText edit_txt = (EditText) findViewById(R.id.loginPass);

        edit_txt.setOnEditorActionListener(new EditText.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId,
                                          KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {
                    LoginSubmit(findViewById(R.id.btnLogin));
                    return true;
                }
                return false;
            }
        });
        // Listening to register new account link
        registerScreen.setOnClickListener(new View.OnClickListener() {

            public void onClick(View v) {
                // Switching to Register screen
                Intent i = new Intent(getApplicationContext(), Register.class);
                startActivity(i);
            }
        });
    }

    public void LoginSubmit (View view){
        logEmail = (EditText)findViewById(R.id.loginEmail);
        logPass = (EditText)findViewById(R.id.loginPass);
        String email = logEmail.getText().toString();
        String pass = logPass.getText().toString();

        SharedPreferences preferences = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
        //SharedPreferences.Editor editor = preferences.edit();
        String storedEmail = preferences.getString("Email", "");
        String storedPass = preferences.getString("Password","");
        if((email.equals("")) || (pass.equals(""))) {
            Toast.makeText(getApplicationContext(), "Please enter Email and Password",
                    Toast.LENGTH_SHORT).show();
        }
        else if((email.equals(storedEmail)) && (pass.equals(storedPass))){
            Toast.makeText(getApplicationContext(), "Correct Credentials",
                    Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(getApplicationContext(), Central.class);
            startActivity(intent);
        }
        else {
            Toast.makeText(getApplicationContext(), "Incorrect Email or Password",
                    Toast.LENGTH_SHORT).show();
        }

    }
}