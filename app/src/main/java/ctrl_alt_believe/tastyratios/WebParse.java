package ctrl_alt_believe.tastyratios;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ListView;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class WebParse extends Activity implements View.OnClickListener {
    Intent intent;
    ListView listView;
    Button useButton;
    ArrayList<Ingr_DB_MDL> ingList;
    ArrayList<Steps_DB_MDL> stepList;
    ArrayList<String> listIngredients,
                      listSteps;
    ArrayAdapter<String> adapterIngredients;
    WebRecipeResult result;
    private String sURL;

    ProgressDialog mProgDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_parse);
        intent = getIntent();
        listView = (ListView) findViewById(R.id.listView);
        useButton = (Button) findViewById(R.id.UseRecipe);
        result = intent.getParcelableExtra("Recipe");
        sURL = result.getRecipeLink();
        listIngredients = new ArrayList<String>();
        listSteps = new ArrayList<String>();

        adapterIngredients = new ArrayAdapter<String>(
                this,
                android.R.layout.simple_expandable_list_item_1,
                listIngredients
        );
        listView.setAdapter(adapterIngredients);
        useButton.setOnClickListener(this);

        new GrabContent().execute();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.web_parse, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        Intent intent = new Intent(getApplicationContext(), FollowRecipe.class);
        Recipe_DB_MDL recipe = new Recipe_DB_MDL(result.getRecipeName());
        recipe.addIngredients(ingList);
        recipe.addSteps(stepList);
        intent.putExtra("Recipe", recipe);
        startActivity(intent);
    }

    private class GrabContent extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgDialog = new ProgressDialog(WebParse.this);
            mProgDialog.setTitle(getResources().getString(R.string.web_parse_progress_title));
            mProgDialog.setMessage(getResources().getString(R.string.web_parse_progress_msg));
            mProgDialog.setIndeterminate(false);
            mProgDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                ingList = new ArrayList<Ingr_DB_MDL>();
                stepList = new ArrayList<Steps_DB_MDL>();
                WebParse_Base allRecipe = new WebParse_AllRecipes(sURL);
                Elements ingredients = allRecipe.getElementIngredients();
                Elements steps = allRecipe.getElementSteps();

                for (Element element : ingredients) {
                    String  display,
                            name,
                            listItem,
                            amount;
                    Matcher m;
                    Document parsed = Jsoup.parse(element.toString());
                    display = parsed.text();
                    name = Jsoup.parse(element.select("#lblIngName").toString()).text();
                    listItem = Jsoup.parse(element.select("#lblIngAmount").toString()).text();
                    if (!display.equals("\u00A0")) {
                        Ingr_DB_MDL addIngr = new Ingr_DB_MDL();
                        addIngr.setName(name);
                        if (listItem.contains("(") && listItem.contains(")") && listItem.substring(listItem.indexOf('('), listItem.indexOf(')')).matches(".*\\d.*")) {
                            amount = listItem.substring(listItem.indexOf('(') + 1, listItem.indexOf(')'));
                        } else {
                            amount = (listItem.length() > 0 ? listItem : "1");
                        }
                        m = Pattern.compile("\\d").matcher(amount);
                        int first = m.find() ? m.start() : 0;
                        int last = 0;
                        while (m.find()) {
                            last = m.start();
                        }
                        addIngr.setAmount(amount.substring(first, last + 1));
                        addIngr.setUnit(amount.substring(last + 1));
                        ingList.add(addIngr);
                        listIngredients.add(display);
                    }
                }

                for (Element element : steps) {
                    String step = Jsoup.parse(element.toString()).text();
                    int delta = stepList.size();
                    delta++;
                    Steps_DB_MDL newStep = new Steps_DB_MDL(delta, step);
                    stepList.add(newStep);
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            mProgDialog.dismiss();
            adapterIngredients.notifyDataSetChanged();
        }
    }
}
