package ctrl_alt_believe.tastyratios;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.AutoCompleteTextView;
import android.widget.Button;
import android.widget.ListView;

import java.util.ArrayList;


public class WebSearch extends Activity implements View.OnClickListener {

    Button searchBtn;
    AutoCompleteTextView searchTxt;
    ListView searchList;
    ArrayList<WebRecipeResult> recipeList;
    ArrayAdapter<WebRecipeResult> arrayAdapter;
    ProgressDialog mProgDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_web_search);

        searchTxt  = (AutoCompleteTextView) findViewById(R.id.searchTxt);
        searchList = (ListView) findViewById(R.id.listView);
        searchBtn  = (Button)   findViewById(R.id.searchBtn);

        recipeList = new ArrayList<WebRecipeResult>();
        arrayAdapter = new ArrayAdapter<WebRecipeResult>(
                this,
                android.R.layout.simple_list_item_1,
                recipeList
        );

        searchList.setAdapter(arrayAdapter);
        searchBtn.setOnClickListener(this);
        searchList.setClickable(true);
        searchList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent intent;
                intent = new Intent(getApplicationContext(), WebParse.class);
                WebRecipeResult selection = recipeList.get(position);
                intent.putExtra("Recipe", selection);
                startActivity(intent);
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.web_search, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    @Override
    public void onClick(View v) {
        String query = searchTxt.getText().toString();
        if (query.length() > 0) {
            InputMethodManager imm;
            imm = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            imm.hideSoftInputFromWindow(searchTxt.getWindowToken(), 0);
            recipeList.clear();
            arrayAdapter.notifyDataSetChanged();
            new GrabContent(query).execute();
        }
    }
    private class GrabContent extends AsyncTask<Void, Void, Void> {
        String results;
        String query;

        public GrabContent(String query) {
            this.query = query;
        }
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            mProgDialog = new ProgressDialog(WebSearch.this);
            mProgDialog.setTitle("Getting recipes.");
            mProgDialog.setMessage("Loading...");
            mProgDialog.show();
        }

        @Override
        protected Void doInBackground(Void... params) {
            WebSearch_AllRecipes search1 = new WebSearch_AllRecipes(query);
            try {
                recipeList.addAll(search1.search());
            }
            catch (Exception e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            mProgDialog.dismiss();
            arrayAdapter.notifyDataSetChanged();
        }
    }
}
